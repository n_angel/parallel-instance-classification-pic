/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "parameter_handler.h"

//// Implemented functions. /////////////////////////////////////////////////
std::map<std::string, std::string> PARAMETER::unpack_parameters(int argc, char *argv[])
{
	std::map<std::string, std::string> params;

	if (argc % 2 == 0)
	{
		std::cout << "Parameters error!" << std::endl;
		exit(-1);
	}

	for (int i = 1; i < argc; i+=2)
	{
		params[std::string(argv[i])] = std::string(argv[i+1]);
	}
	
    return params;
}

std::string PARAMETER::get_value(std::map<std::string, std::string> params, std::string flag)
{
	std::map<std::string, std::string>::iterator it;
	if ( (it = params.find(flag)) != params.end() ) {
	  return it->second;
	} 
	else 
	{
		perror(("The param" + flag + " doesn't exist").c_str());
		exit(-1);
	}
}