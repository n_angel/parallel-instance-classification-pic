/*
 * DifferentialEvolution.h
 *
 */


#ifndef CLUSTERING_H
#define CLUSTERING_H


//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <iostream>
# include <vector>
# include <map>
# include <fstream>
# include <string>
# include <iostream>
# include <string>
# include <sstream>
# include <algorithm>
# include <iterator>
# include <map>
# include <math.h>
# include <ctime>
# include <limits>
# include <omp.h>

# include "definitions.h"
# include "iodata.h"

//// Global Vars	 //////////////////////////////////////////////////////// 
//// Used namespaces. ///////////////////////////////////////////////////////
//// New includes. //////////////////////////////////////////////////////////

bool sort_pattern(const PATTERN &p1, const PATTERN &p2);

namespace CLUSTERING
{
	//void shuffle_dataset(std::vector<PATTERN>::iterator d_begin, std::vector<PATTERN>::iterator d_end);

	std::vector< CLUSTER > kmeans(int k, std::vector<PATTERN>::iterator d_begin, 
													int s_patterns,
													double error_optm,
													int max_itrs);
	double vDistance(std::vector<double> v1, std::vector<double> v2);

	std::vector< CLUSTER > parallel_kmeans(int threads, 
											int k, 
											std::vector<PATTERN>::iterator d_begin, 
											std::vector<PATTERN>::iterator d_end,
											double error_optm,
											int max_itrs);

	double test(std::vector<PATTERN>::iterator d_begin, std::vector<PATTERN>::iterator d_end,
				std::vector<CLUSTER>::iterator c_begin, std::vector<CLUSTER>::iterator c_end);

}

# endif
