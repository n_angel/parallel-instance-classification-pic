/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "iodata.h"
 
 std::map<std::string, int> klasses;

//// Implemented functions. /////////////////////////////////////////////////

std::vector< PATTERN > INOUTDATA::read_data(std::string namefile)
{
	std::random_device rd;
    std::mt19937 g(rd());

	std::vector< PATTERN > data;
	std::map<std::string, int>::iterator kls;

	std::ifstream infile(namefile);
	std::string line;
	while (std::getline(infile, line))
	{
		std::istringstream iss(line);
		std::vector<std::string> tokens;
		std::copy(std::istream_iterator<std::string>(iss),
		     std::istream_iterator<std::string>(),
		     std::back_inserter(tokens));

		PATTERN nw;
		for (int it_i = 0; it_i < tokens.size()-1; ++it_i)
		{
			nw.vars.push_back(std::stod(tokens[it_i]));
		}

		kls = klasses.find(tokens[tokens.size()-1]);
		if (kls != klasses.end())
		{
			nw.klass = kls->second;
		}
		else
		{
			klasses[tokens[tokens.size()-1]] = klasses.size();
			nw.klass = klasses.size() - 1;
		}

		data.push_back(nw);
	}

	//std::shuffle(data.begin(), data.end(), g);
	return data;
}

void INOUTDATA::printc(std::vector<PATTERN>::iterator d_begin, std::vector<PATTERN>::iterator d_end)
{
	while(d_begin != d_end)
	{
		for (int it_i = 0; it_i < (int)d_begin->vars.size(); ++it_i)
		{
			std::cout << d_begin->vars[it_i] << "\t";
		}
		std::cout << d_begin->klass << std::endl;
		d_begin ++;
	}
}


void INOUTDATA::printc(std::vector<CLUSTER>::iterator d_begin, std::vector<CLUSTER>::iterator d_end)
{
	double t_patterns = 0;
	while(d_begin != d_end)
	{
		std::cout << "\n\n*********\nClass: " << d_begin->centroid.klass << "\nCentroid:" << std::endl;
		for (int it_v = 0; it_v < d_begin->centroid.vars.size(); ++it_v)
		{
			std::cout << d_begin->centroid.vars[it_v] << "\t";
		}
		std::cout <<"\n\t\t\t"<<  d_begin->ids.size() << " PATTERNS" << std::endl; 
		t_patterns += (int)d_begin->ids.size();
		std::cout << "Id patterns:" << std::endl;
		for (int it_p = 0; it_p < d_begin->ids.size(); ++it_p)
		{
			std::cout << d_begin->ids[it_p] << "\t";
		}

		d_begin ++;
	}
	std::cout << "\n\t\t Total de patrones: " << t_patterns << std::endl;
}