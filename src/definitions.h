/*
 * DifferentialEvolution.h
 *
 */


#ifndef DEFINITIONS_H
#define DEFINITIONS_H


//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <iostream>
# include <vector>
# include <map>
# include <fstream>
# include <string>
# include <iostream>
# include <string>
# include <sstream>
# include <algorithm>
# include <iterator>
# include <map>

//// Global Vars	 //////////////////////////////////////////////////////// 
//// Used namespaces. ///////////////////////////////////////////////////////
//// New includes. //////////////////////////////////////////////////////////

typedef struct pattern
 {
 	double klass = -1;
 	std::vector<double> vars;
 }PATTERN;

 typedef struct cluster
 {
 	PATTERN centroid;
 	std::vector< int > ids;
 }CLUSTER;

# endif