/*
 * DifferentialEvolution.h
 *
 */


#ifndef IODATA_H
#define IODATA_H


//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <iostream>
# include <vector>
# include <map>
# include <fstream>
# include <string>
# include <iostream>
# include <string>
# include <sstream>
# include <algorithm>
# include <iterator>
# include <map>

# include "definitions.h"

//// Global Vars	 //////////////////////////////////////////////////////// 
//// Used namespaces. ///////////////////////////////////////////////////////
//// New includes. //////////////////////////////////////////////////////////


namespace INOUTDATA
{
	
	std::vector< PATTERN > read_data(std::string namefile);	
	void printc(std::vector<PATTERN>::iterator d_begin, std::vector<PATTERN>::iterator d_end);
	void printc(std::vector<CLUSTER>::iterator d_begin, std::vector<CLUSTER>::iterator d_end);
}  // Parameter Handler namespace


# endif