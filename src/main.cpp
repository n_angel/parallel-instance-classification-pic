// standard libraries
# include <iostream>
# include <cstdio>
# include <vector>
# include <random>
# include <algorithm>
# include <string>
# include <ctime>
# include <omp.h>

# include "definitions.h"

// new libraries
# include "parameter_handler.h"
# include "iodata.h"
# include "clustering.h"

// Main
int main(int argc, char *argv[])
{
    std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);
    std::string train_file = PARAMETER::get_value(params, "-train");
    std::string test_file = PARAMETER::get_value(params, "-test");
    int k = std::stoi(PARAMETER::get_value(params, "-k"));
    double error_optm = std::stod(PARAMETER::get_value(params, "-error"));
	int max_itrs = std::stoi(PARAMETER::get_value(params, "-maxitrs"));
    int threads = std::stoi(PARAMETER::get_value(params, "-threads"));

    std::vector< PATTERN > data_train = INOUTDATA::read_data(train_file);
    std::vector< PATTERN > data_test = INOUTDATA::read_data(test_file);

    /*
     std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    for (int it_p = 0; it_p < (int)data.size(); ++it_p)
    {
    	std::cout << it_p << "\tclass: " << data[it_p].klass << std::endl;
    }

    std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
    */

    std::vector< CLUSTER > clusters;
    if( threads == 1)
    {
	double start_time = omp_get_wtime();
    	clusters = CLUSTERING::kmeans(k, data_train.begin(), (int)data_train.size(), error_optm, max_itrs);
	double time = omp_get_wtime() - start_time;
	std::cout << "\n" << time << " seconds elapsed" <<  std::endl;
    }
    else if(threads >= 2)
    {
	clusters = CLUSTERING::parallel_kmeans(threads, k, data_train.begin(), data_train.end(), error_optm, max_itrs);
    }

    double error = CLUSTERING::test(data_test.begin(), data_test.end(), clusters.begin(), clusters.end());
    std::cout << "\n\n" << error << " error" << std::endl;

    return 0;
}
