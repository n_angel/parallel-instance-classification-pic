/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "clustering.h"

//// Implemented functions. /////////////////////////////////////////////////

/*
	kmeans para un conjunto de patrones
*/
std::vector< CLUSTER > CLUSTERING::kmeans(int k, std::vector<PATTERN>::iterator d_begin, 
													int s_patterns,
													double error_optm,
													int max_itrs)
{
	std::random_device rd;
    std::mt19937 g(rd());

	std::vector< CLUSTER > clusters;

	// Para no repetir centroides aleatorios
	std::vector< int > ids;
	for (int i = 0; i < s_patterns; ++i) ids.push_back(i);
	std::shuffle(ids.begin(), ids.end(), g);

	for (int it_k = 0; it_k < k; ++it_k)
	{
		clusters.push_back({*(d_begin+ids[it_k]), std::vector< int>(0)});
	}

	double dist_min, dist_tmp;
	int cluster_min;
	int c_itr = 0;
	double last_error = 1;
	while(true)
	{
		/*
		*****************************************************************************
			Proceso de clusterizacion
		*****************************************************************************
		*/
		// calcular la distancia de los puntos restantes
		for (int it_p = 0; it_p < s_patterns; ++it_p)	// Para cada punto
		{
			dist_min = std::numeric_limits<double>::max();
			cluster_min = 0;
			for (int it_c = 0; it_c < (int)clusters.size(); ++it_c)	// Para cada cluster
			{
				dist_tmp = vDistance(clusters[it_c].centroid.vars, (d_begin+it_p)->vars);
				if (dist_tmp < dist_min)
				{
					dist_min = dist_tmp;
					cluster_min = it_c;
				}
			}

			clusters[cluster_min].ids.push_back(it_p);	// se agrega al cluster con el cual tenga menor distancia a su centroide
		}

		int clss;
		int max_class;
		int max_elments;
		int elements;
		double error = 0.0;	// Nos interesa conocer el error global,  debido a eso se cuenta de todos los patrones cuantos estan mal clasificados
		std::vector<int> bucket;
		// Calcula el error de la clasificación
		for (int it_k = 0; it_k < clusters.size(); ++it_k)
		{
			elements = (int)clusters[it_k].ids.size();
			max_class = 0;
			max_elments = 0;
			bucket = std::vector<int> (clusters.size(), 0);	// Cubeta para contar cuantos elementos hay de cada clase en el cluster
			// ciclo para determinar la clase de mayor precencia en el cluster
			for (int it_e = 0; it_e < elements; ++it_e)
			{
				clss = (d_begin+(clusters[it_k].ids[it_e]))->klass;
				bucket[clss] ++;
				if (bucket[clss] > max_elments)
				{
					max_elments  = bucket[clss];
					max_class = clss;
				}
			}

			clusters[it_k].centroid.klass = max_class;
			// cuenta cuantos elemntos son una clase diferente a la predominante en el grupo, de ese modo calcula el error
			for (int it_e = 0; it_e < elements; ++it_e)
			{
				clss = (d_begin+(clusters[it_k].ids[it_e]))->klass;
				if (clss != max_class)	// Si el patron pertenece a una clase diferente a la predominante en el cluster
				{
					error ++;
				}
			}
		}

		c_itr  ++;
		/*
		*****************************************************************************
			validacion de convergencia
		*****************************************************************************
		*/
		if (c_itr >= max_itrs || std::fabs(last_error - (error / (double)s_patterns)) < error_optm)
		{
			break;
		}
		last_error = (error / (double)s_patterns);

		/*
		*****************************************************************************
			Calculo de nuevos centroides
		*****************************************************************************
		*/
		int s_vars;
		for (int it_k = 0; it_k < clusters.size(); ++it_k)
		{
			s_vars = (int) clusters[it_k].centroid.vars.size();

			clusters[it_k].centroid.vars.clear( );	// elima el valor de los centroides anteriores
			clusters[it_k].centroid.vars = std::vector<double>(s_vars, 0.0);

			elements = (int)clusters[it_k].ids.size();
			for (int it_e = 0; it_e < elements; ++it_e)	// Para cada uno de los patrones listados en el cluster it_k
			{
				for (int it_v = 0; it_v < s_vars; ++it_v)	// para cada una de las variables de un patron del cluster it_k
				{
					clusters[it_k].centroid.vars[it_v] += (d_begin+(clusters[it_k].ids[it_e]))->vars[it_v];
				}
			}

			for (int it_v = 0; it_v < s_vars; ++it_v)	// centroide: media de cada una de las variables de los elemntos del cluster
			{
				clusters[it_k].centroid.vars[it_v] /= (double)elements;
			}

			clusters[it_k].centroid.klass = -1;
			clusters[it_k].ids.clear( );	// elimina los elementos de los clusters
		}
	}

	return clusters;
}

/*
Retorna la distancia Euclidiana entre dos vecotres
*/
double CLUSTERING::vDistance(std::vector<double> v1, std::vector<double> v2)
{
	int dim = v1.size( );
	double sum = 0.0;
	for (int it_i = 0; it_i < dim; ++it_i)
	{
		sum += (v1[it_i] - v2[it_i]) * (v1[it_i] - v2[it_i]);
	}

	return sqrt(sum);
}

bool sort_pattern(const PATTERN &p1, const PATTERN &p2)
{
	int it_i = 0;
	while ( it_i < (int)p1.vars.size()-1 && (int)p1.vars[it_i] == (int)p2.vars[it_i]) it_i ++;

	return (int)p1.vars[it_i] < (int)p2.vars[it_i];
}

std::vector< CLUSTER > CLUSTERING::parallel_kmeans(int threads, 
													int k, 
													std::vector<PATTERN>::iterator d_begin, 
													std::vector<PATTERN>::iterator d_end,
													double error_optm,
													int max_itrs)
{
	//std::vector< CLUSTER > clusters = ;
	std::vector< CLUSTER > tmp_clusters;
	int s_patterns = std::distance(d_begin, d_end);

	std::vector<bool> flag = std::vector<bool> (s_patterns, false);

	std::sort(d_begin, d_end, sort_pattern);

	int id_start;
	int id_end;
	int chunk = (double)s_patterns / (double)threads;
	int it_c, it_v, id_p;
	int pointer = 0;
	std::vector< CLUSTER > clusters = std::vector< CLUSTER >(threads * k);
	tmp_clusters.clear(  );

	// *****************************************************************************************
	// Division del conjunto de datos a los p-hilos
	omp_set_num_threads(threads);
	double start_time = omp_get_wtime();
	#pragma omp parallel default(none) \
			     firstprivate(k, chunk, threads, s_patterns, d_begin, error_optm, max_itrs) \
			     private(it_c, it_v, id_p, id_start, id_end, tmp_clusters) \
			     shared(pointer, clusters)
	//for (int id_p = 0; id_p < threads; ++id_p)
	{
		id_p = omp_get_thread_num();
		id_start = id_p * chunk;
		id_end = id_start + chunk - 1;
		id_end = (id_p == threads-1)? s_patterns-1 : id_end;
		
		tmp_clusters.clear(  );
		tmp_clusters = CLUSTERING::kmeans(k, d_begin+id_start, (id_end - id_start) + 1, error_optm, max_itrs);

		for (it_c = 0; it_c < (int)tmp_clusters.size(  ); ++it_c)
		{
			for (it_v = 0; it_v < (int)tmp_clusters[it_c].ids.size(); ++it_v)
			{
				tmp_clusters[it_c].ids[it_v] += id_start;
			}
		}

		// encolar los k closter producidos por este hilo
		#pragma omp critical
		{
			for (it_c = 0; it_c < (int)tmp_clusters.size(); ++it_c)
			{
				//clusters.push_back(tmp_clusters[it_c]);
				clusters[pointer] = tmp_clusters[it_c];
				pointer ++;
			}
		}
	}

	#pragma omp barrier
	double time_elp = omp_get_wtime() - start_time;
	std::cout << "\n" << time_elp << " seconds elapsed" <<  std::endl;
	// *****************************************************************************************

	//exit(-1);
	//std::cout << clusters.size(  ) << " clusters en la cola" << std::endl;
	//std::cout << "\n\nRESULTADO FINAL: " << std::endl;
	//INOUTDATA::printc(clusters.begin( ), clusters.end( ));

	// ** Termina sección paralela
	/*	
		************************************************************
		Merge de los clusters
		***********************************************************
	*/
	int s_clusters = clusters.size(  );
	while(s_clusters > k)
	{
		double min_dist = std::numeric_limits<double>::max();
		double min_centroid_id = 0;
		double dist_tmp;
		for (int it_c = 0; it_c < s_clusters-1; ++it_c)
		{	// Compara cada uno de los {0, ..., N-1} clusters contra el N-esimo cluster para hacer el merge
			dist_tmp = vDistance(clusters[it_c].centroid.vars, clusters[s_clusters-1].centroid.vars);
			if (dist_tmp < min_dist)
			{
				min_dist = dist_tmp;
				min_centroid_id = it_c;
			}

		}
		// merge con el cluster mas cercano (copia los patrones del cluster N al cluster mas cercano)
		for (int it_p = 0; it_p < (int)clusters[s_clusters-1].ids.size(); ++it_p)
		{
			clusters[min_centroid_id].ids.push_back(clusters[s_clusters-1].ids[it_p]);
		}

		int aux_id_p;

		// Recalcula el centroide del conjunto
		int s_vars = (int)clusters[min_centroid_id].centroid.vars.size(  );

		int max_class = -1;
		int id_max_class = 0;
		std::vector<int> bucket = std::vector<int> (clusters.size(), 0);
		clusters[min_centroid_id].centroid.vars = std::vector<double> (s_vars, 0.0);	// incializa el valor del centroide
		clusters[min_centroid_id].centroid.klass = -1;	//inicializa la clase predominante
		for (int it_p = 0; it_p < (int)clusters[min_centroid_id].ids.size(); ++it_p)	// Para cada patron del nuevo cluster
		{
		 	 aux_id_p = clusters[min_centroid_id].ids[it_p];
		 	 for (int it_v = 0; it_v < s_vars; ++it_v)	// Para cada variable de: centroide y patrones
		 	 {
		 	 	clusters[min_centroid_id].centroid.vars[it_v] += (d_begin+aux_id_p)->vars[it_v];
		 	 }
		 	 bucket[(d_begin+aux_id_p)->klass] ++;	// cuenta el numero de elementos por clase en cada cluster
		 	 if (max_class < bucket[(d_begin+aux_id_p)->klass])	// encuentra la clase con mayor numero de elementos
		 	 {
		 	 	max_class = bucket[(d_begin+aux_id_p)->klass];
		 	 	id_max_class = (d_begin+aux_id_p)->klass;
		 	 }
		}

		for (int it_v = 0; it_v < s_vars; ++it_v)
		{
			clusters[min_centroid_id].centroid.vars[it_v] /= (double)clusters[min_centroid_id].ids.size();
		}
		clusters[min_centroid_id].centroid.klass = id_max_class;
		
		// Elimina el cluster que se unio
		clusters.pop_back( );
		s_clusters = clusters.size(  );
	}

	//INOUTDATA::printc(clusters.begin( ), clusters.end( ));

	return clusters;
}

/* 
	Asigna una clase a cada conjunto de prueba en base a un conjunto de cluster creados con el conjunto de entrenamiento
*/
double CLUSTERING::test(std::vector<PATTERN>::iterator d_begin, std::vector<PATTERN>::iterator d_end,
						std::vector<CLUSTER>::iterator c_begin, std::vector<CLUSTER>::iterator c_end)
{
	int s_patterns = std::distance(d_begin, d_end);
	int s_clusters = std::distance(c_begin, c_end);

	double error = 0.0;

	double dist_tmp;
	double dist_min;
	int class_min;
	for (int it_p = 0; it_p < s_patterns; ++it_p)	// Para cada patron del conjunto de prueba
	{
		dist_min = std::numeric_limits<double>::max();
		class_min = -1;
		for (int it_c = 0; it_c < s_clusters; ++it_c)	// Para cada cluster
		{
			dist_tmp = vDistance((c_begin+it_c)->centroid.vars, (d_begin+it_p)->vars);
			if (dist_tmp < dist_min)
			{
				dist_min = dist_tmp;
				class_min = (c_begin+it_c)->centroid.klass;
			}
		}

		if (class_min != (d_begin+it_p)->klass)	// Comparala clase en que agrupa contra la verdad
		{
			error ++;
		}
	}

	return error / (double)s_patterns;
}

