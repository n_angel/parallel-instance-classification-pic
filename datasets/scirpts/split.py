#!/usr/bin/python

import  numpy as np
import random
from random import shuffle
import sys

def concat(path, aux_name):
    out = ""
    buff = path.split("/")
    for st in buff[:-1]:
        print st
        out += str(st + "/")

    return out + aux_name + "_" + buff[-1]


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "Error en el numero de parametros es necesario"
        print "Dataset"
        print "procentage de split (procentage para formar el conjunto test)"
        exit(-1)

    path = sys.argv[1]
    print "Path: ", path
    per = float(sys.argv[2])
    print "Percentage to test: ", per

    fl = open(path, "r")
    data = fl.readlines()
    fl.close()

    sets = {}

    for l in data:
        #print l
        buff = l.split()
        klass = buff[-1]
        #print klass

        if klass not in sets:
            sets[klass] = [buff]
        else:
            sets[klass].append(buff)

        #print "size of set: ", len(sets[klass])

    n_train = concat(path, "train")
    n_test = concat(path, "test")


    f_train = open(n_train, "w")
    print "test: ", n_test
    f_test = open(n_test, "w")

    ctr = 0
    for cls in sets:
        #print cls
        #print "size: ", len(sets[cls])
        #print sets[cls]
        ids = range(0, len(sets[cls]))
        shuffle(ids)
        #print ids

        id_start_train = 0
        id_end_train = int(len(sets[cls]) * (1.0-per))

        id_start_test = int(id_end_train + 1)
        id_end_test = int(len(sets[cls]))

        print "\ntrain: ", n_train
        for it in range(id_start_train, id_end_train+1):
            print ctr, "\t", ids[it],"\t",
            print ' '.join(map(str, sets[cls][ids[it]]))
            f_train.write(' '.join(map(str, sets[cls][ids[it]])) + "\n")
            ctr += 1

        print "\ntest: ", n_test
        for it in range(id_start_test, id_end_test):
            print ctr, "\t", ids[it],"\t",
            print ' '.join(map(str, sets[cls][ids[it]]))
            f_test.write(' '.join(map(str, sets[cls][ids[it]])) + "\n")
            ctr += 1

        print "total: ", ctr

    f_test.close()
    f_train.close()
